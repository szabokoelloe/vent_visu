from setuptools import setup, find_packages

setup(
    name='vent_visu',
    description='Create plots for Vent AWS',
    version='1.0.2',
    author='Wolfgang Gurgiser',
    author_email='wolfgang.gurgiser@uibk.ac.at',
    packages=find_packages(),
    install_requires=[
        'bokeh',
        'numpy',
        'pandas'
    ],
    entry_points={
        'console_scripts': [
            'plot_vent=vent.cli:main'
        ]
    }
)
