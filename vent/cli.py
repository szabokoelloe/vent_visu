import argparse
import os
import pandas as pd
from contextlib import contextmanager
from vent.plot import plot1

def drop_parens(str):
    return str.replace('(', '').replace(')', '')

def normalize_path(path):
    return os.path.abspath(os.path.expanduser(path))

@contextmanager
def working_directory(path):
    cwd = os.getcwd()
    norm_path = normalize_path(path)
    try:
        os.chdir(norm_path)
        yield
    finally:
        os.chdir(cwd)

# 60days, 24hours, 60minutes
def read_data(path, max_rows=60 * 24 * 60 ):
    df = pd.read_csv(path, delimiter=',', skiprows=[2, 3], header=1,
                       index_col=0, parse_dates=True, na_values=["NAN", "NaN"])
    df.rename(drop_parens, axis='columns', inplace=True)
    #df = df.resample('10min').asfreq() ## gurgiser want 1min data
    if df.shape[0] > max_rows:
        df = df.truncate(before=df.index[df.shape[0]-max_rows])
    return df

def main():
    parser = argparse.ArgumentParser(description='Create Plots for Tisenjoch AWS')
    parser.add_argument('-o', '--output-dir', default=os.getcwd(), help='Output directory')
    parser.add_argument('data_file', help='Path to the aws data file')
    args = parser.parse_args()
    data_path = normalize_path(args.data_file)
    with working_directory(args.output_dir):
        data = read_data(data_path)
        plot1(data)
