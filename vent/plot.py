# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 09:39:33 2018

@author: Wolfgang Gurgiser, Christian Posch
"""

import numpy as np
import pandas as pd
import math
from math import pi
from datetime import timedelta
from bokeh.io import output_file, save
from bokeh.layouts import gridplot
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource,DatetimeTickFormatter, NumeralTickFormatter, HoverTool, LinearAxis, Range1d
from bokeh.models.glyphs import Rect
from bokeh.models import Label

def calc_diff_last_data_2_now(df):
    return pd.Timestamp.now() - df.index[-1]

def style_figure(f):
    f.background_fill_color = "black"
    f.border_fill_color="black"
    f.axis.axis_line_color = "white"
    f.grid.grid_line_color = "#595959"
    f.axis.major_label_text_color = "white"
    f.axis.axis_label_text_color = "white"
    f.title.text_color="white"
    f.legend.background_fill_color="black"
    f.legend.background_fill_alpha = 1
    f.legend.border_line_color = "black"
    f.legend.label_text_color = "white"
    f.legend.location = "bottom_left"
    f.xaxis.formatter=DatetimeTickFormatter(
        hours=["%d %B %Y"],
        days=["%Y %m %d"],
        months=["%d %B %Y"],
        years=["%Y"],
    )
    f.xaxis.major_label_orientation = pi/4

def plot1(df):
    output_file('overview_aws_vent_3days.html')
    delta=-60 * 3 * 24
    f1 = plot1_figure1(df, delta)
    f2 = plot1_figure2(df, delta)
    f3 = plot1_figure3(df, delta)
    f4 = plot1_figure4(df, delta)
    grid = gridplot([f1, f2, f3, f4], ncols=2, plot_width=600, plot_height=600)
    save(grid)
    from bokeh.plotting import show
    show(grid)

def hover_tool():
    return HoverTool(tooltips=[
        ('Date', '@x{%Y %m %d %Hh}'),
        ('Value', '@y{1.11}')], formatters={'x': 'datetime'}, mode='mouse')

def create_figure(label, df, delta):
    f = figure(plot_width=300, plot_height=300, x_axis_type='datetime',
               title=label)
    f.x_range.start = pd.Timestamp.now() - timedelta(days=3)
    f.x_range.end = pd.Timestamp.now()
    return f

def calc_font_size(time_diff):
    if time_diff > timedelta(hours=10):
        return '20pt'
    return '10pt'

def add_blue_rect(f, df, time_diff, mmin, mmax, height):
    middle = (mmax + mmin)/2
    width = calc_diff_last_data_2_now(df)/2
    f.rect([df.index[-1]+width], [0], time_diff, height, fill_alpha=0.25)
    l = Label(x=pd.Timestamp.now()-timedelta(hours=1), y=middle, text='NO DATA', 
            text_font_size=calc_font_size(time_diff), 
            text_color='white', text_align="center", 
            text_alpha=0.80, angle=90, angle_units='deg')
    f.add_layout(l)

def add_total_rain_label(f, val12, val24, posx=480, posy=480):
    mfontsize = '10pt'
    secposdiff = 25
    l = Label(x=posx, y=posy, text='Total precipitation last 12h: {:.2f} mm'.format(round(val12,2)), 
            text_font_size=mfontsize, x_units='screen', y_units='screen',
            text_color='yellow', text_align="right", 
            text_alpha=0.80, angle=0, angle_units='deg')
    f.add_layout(l)
    l = Label(x=posx, y=posy-secposdiff, text='Total Precipitation last 24h: {:.2f} mm'.format(round(val24,2)), 
            text_font_size=mfontsize, x_units='screen', y_units='screen',
            text_color='yellow', text_align="right", 
            text_alpha=0.80, angle=0, angle_units='deg')
    f.add_layout(l)

def calc_rain_total(df, mhours):
    #print(df.index[-1*mhours*60])
    return sum(df[-1*mhours*60:].accumulated_rt_nrt)

def calc_real_range(df, delta):
    mdays = math.ceil(delta*-1/24/60)
    time_diff = calc_diff_last_data_2_now(df)
    time_diff_in_data = timedelta(days=mdays) - time_diff
    diff_4_range_check = math.ceil(time_diff_in_data.days * 24 * 60 + time_diff_in_data.seconds/60) * -1
    return diff_4_range_check

def plot1_figure1(df, delta):
    diff_4_range_check = calc_real_range(df, delta)
    
    f = create_figure('Temperature and Humidity', df, delta)
    t_minima = df[diff_4_range_check:].ta_1.min() 
    t_maxima = df[diff_4_range_check:].ta_1.max()
    f.y_range = Range1d(start=math.floor(t_minima - 1), end=math.ceil(t_maxima + 1))
    f.extra_y_ranges = {'Humidity': Range1d(start=df.rh_1.min(), end=df.rh_1.max())}
    f.add_tools(hover_tool())
    f.line(df.index, df.ta_1, line_width=2, color='white', legend_label='T Air')
    f.line(df.index, df.rh_1, line_width=2, y_range_name="Humidity", line_dash='dashed', color='#27f7f3', legend_label='RH')
    f.yaxis.axis_label = 'T'
    f.add_layout(LinearAxis(y_range_name='Humidity', axis_label='%'), 'right')   
 
    if calc_diff_last_data_2_now(df) > timedelta(hours=2):
        add_blue_rect(f, df, calc_diff_last_data_2_now(df), t_minima.min(), t_maxima.max(), 210)

    style_figure(f)
    return f

def plot1_figure2(df, delta):
    diff_4_range_check = calc_real_range(df, delta)
    f = create_figure('Wind', df, delta)
    f.extra_y_ranges = {'Wind_dir': Range1d(start=-0.5, end=365)}
    f.y_range = Range1d(start=-0.5, end=df[diff_4_range_check:].ws_ms_s_wvt_1.max() + 0.5)
    f.add_tools(hover_tool())
    f.circle(df.index, df.winddir_d1_wvt_1, radius=0.5, radius_dimension='max', radius_units='data', color='#27f7f3', y_range_name='Wind_dir',
             legend_label='Wind dir', name='Wind_dir')
    f.line(df.index, round(df.ws_ms_s_wvt_1*100)/100, line_width=1, color='grey', legend_label='ws')
    f.yaxis.axis_label = 'm/s'
    f.add_layout(LinearAxis(y_range_name='Wind_dir', axis_label='degree'), 'right')

    if calc_diff_last_data_2_now(df) > timedelta(hours=2):
        add_blue_rect(f, df, calc_diff_last_data_2_now(df), 0, df.ws_ms_s_wvt_1[delta:].max(), 510)

    style_figure(f)
    return f

def plot1_figure3(df, delta):
    diff_4_range_check = calc_real_range(df, delta)

    f = create_figure("Precipitation intensity", df, delta)
    f.y_range = Range1d(start=-0.1, end=df[diff_4_range_check:].accumulated_rt_nrt.max() + 0.2)
    f.add_tools(hover_tool())
    f.line(df.index, df.accumulated_rt_nrt, line_width=2, color='#27f7f4', legend_label='precipitation')
    f.yaxis.axis_label = 'mm/min'

    if calc_diff_last_data_2_now(df) < timedelta(minutes=80): # 80min beacause, 1hour timediff+20min update sourcedatafile
        add_total_rain_label(f, calc_rain_total(df, 12), calc_rain_total(df, 24))

    if calc_diff_last_data_2_now(df) > timedelta(hours=2):
        add_blue_rect(f, df, calc_diff_last_data_2_now(df), 0, df.accumulated_rt_nrt[delta:].max(), 510)

    style_figure(f)
    return f

def plot1_figure4(df, delta):
    diff_4_range_check = calc_real_range(df, delta)
    
    f = create_figure('Snow depth / Pressure', df, delta)
    f.extra_y_ranges = {"Pressure": Range1d(math.floor(df[diff_4_range_check:].p.min()-2), math.ceil(df[diff_4_range_check:].p.max()+2))}
    f.y_range = Range1d(df[diff_4_range_check:].snow_depth.min()-0.2, df[diff_4_range_check:].snow_depth.max()+0.2)
    f.add_tools(hover_tool())
    f.line(df.index, round(df.p*100)/100, color="#27f7f4", y_range_name="Pressure",legend_label="Pressure",name="Pressure")
    f.line(df.index, df.snow_depth, line_width=1,color="white",legend_label="Snow depth")
    f.yaxis.axis_label = "m"
    f.add_layout(LinearAxis(y_range_name="Pressure", axis_label='hPa'), 'right')

    if calc_diff_last_data_2_now(df) > timedelta(hours=2):
        add_blue_rect(f, df, calc_diff_last_data_2_now(df), df[diff_4_range_check:].snow_depth.min()-0.1, df[diff_4_range_check:].snow_depth.max()+0.1, math.ceil(df[diff_4_range_check:].p.max()*2.1))

    style_figure(f)
    return f
